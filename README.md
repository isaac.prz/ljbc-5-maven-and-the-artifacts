# Ljbc-5 maven and the artifacts

Learn java by challenge lesson 5: discover the java maven ecosystem and how to easily reuse the third-party libraries in your project



## Getting started

### Checkout this Git repository
 * clone the repository `git clone git@gitlab.com:isaac.prz/ljbc-5-maven-and-the-artifacts.git`
 * import it as **existing maven project** 
 
### Maven

Take a look at [What is maven?](https://maven.apache.org/what-is-maven.html) 

summarized if you want to reuse other people's code or share your own, you can pack you code into a [JAR (Java ARchive)](https://en.wikipedia.org/wiki/JAR_(file_format)) and then import it using the [pom.xml](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html)


we can declare in the pom.xml via dependencies other jars that our project requires:
 * [commons-math3](https://commons.apache.org/proper/commons-math/) utils for common mathematical operations
 * [log4j](https://logging.apache.org/log4j/2.x/) configurable logging system, to get rid of the System.out.println. You can configure it on the /src/main/resources/log4j2.xml

 ![Run pom-with-dependencies](docs/pom-with-dependencies.png)
 
 As you see you can import these classes from the third-party libraries into your code:
 
 ![Run math-and-log4j](docs/import-math-and-log4j.png)
 
## Challenges

1. look in internet for a library that you can configure in maven to generate an ascii banner and configure it in your pom.xml
1. using the previous library generate an ascii banner with your name 
1. try to configure [junit (version 5)](https://junit.org/junit5/docs/current/user-guide/) in [maven](https://junit.org/junit5/docs/current/user-guide/#running-tests-build-maven) and create a test class, that will test some of your code, or at least call it 


## Overall Information about  _Learn java by challenge_
 This is a set of projects or challeges, ordered in order to serve as a lesson. Every lesson will progress you into your goal, learning java with focus on enterprise practises. 

## Contributing
 Feel free to contribute any commentary, constructive critique, or PR with proposed changes is wellcome

## Authors and acknowledgment
 * Isaac Perez (main author)

## License

Copyright 2021 Isaac Perez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information [MIT License](https://opensource.org/licenses/MIT)

