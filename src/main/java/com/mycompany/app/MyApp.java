package com.mycompany.app;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyApp {

	private static final Logger LOGGER = LogManager.getLogger(MyApp.class);

	public static void main(String[] args) {
		double[] values = new double[] { 0.0, 10.2, 20.4, 30.6, 40.8, 51.0, 61.2, 71.4, 81.6, 91.8, 102.0 };
		printStatistics(values);

		double[] values2 = new double[] { 0.0, 10.2, 20.4, 30.6, 40.8, 51.0, 61.2, 71.4, 81.6, 91.8, 102.0,
				1000.9999999 };
		printStatistics(values2);
	}

	static void printStatistics(double[] values) {
		DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics();
		for (double v : values) {
			descriptiveStatistics.addValue(v);
		}

		double mean = descriptiveStatistics.getMean();
		double median = descriptiveStatistics.getPercentile(50);
		double standardDeviation = descriptiveStatistics.getStandardDeviation();
		double skewness = descriptiveStatistics.getSkewness();
		LOGGER.info(" Mean: {}, Median : {}, Standard deviation : {}, Skewness : {}  ", mean, median, standardDeviation,
				skewness);

	}

}
